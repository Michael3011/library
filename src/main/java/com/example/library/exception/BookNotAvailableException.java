package com.example.library.exception;

public class BookNotAvailableException extends Exception {

    public BookNotAvailableException() {
        super("There are no books with this title left. Check it again later.");
    }
}
