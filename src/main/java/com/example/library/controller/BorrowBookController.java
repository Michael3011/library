package com.example.library.controller;

import com.example.library.exception.BookNotAvailableException;
import com.example.library.service.BookService;
import com.example.library.entity.Book;
import com.example.library.entity.User;
import com.example.library.repository.BookRepository;
import com.example.library.repository.UserRepository;
import com.example.library.service.DateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
public class BorrowBookController {
    @Autowired
    BookRepository books;
    @Autowired
    UserRepository users;
    @Autowired
    BookService bookService;
    @Autowired
    DateService dateService;

    @PreAuthorize("hasAuthority('ROLE_administrator') or hasAuthority('ROLE_librarian') or hasAuthority('ROLE_reader')")
    @PutMapping("/users/{userId}/books/{bookTitle}/borrow")
    public void borrowBook(@PathVariable("userId") int userId, @PathVariable("bookTitle") String bookTitle) throws BookNotAvailableException {
        int id = bookService.getBookId(bookTitle);
        Book book = books.findById(id);

        User user = users.findById(userId);

        Date borrowDate = dateService.addBorrowDate();
        Date returnDate = dateService.addReturnDate(borrowDate);

        book.setBorrowDate(borrowDate);
        book.setReturnDate(returnDate);
        book.setHolder(user);

        books.save(book);
    }

    @PreAuthorize("hasAuthority('ROLE_administrator') or hasAuthority('ROLE_librarian') or hasAuthority('ROLE_reader')")
    @PutMapping("/users/{userId}/books/{bookId}/return")
    public void returnBook(@PathVariable("userId") int userId, @PathVariable("bookId") int bookId) {
        Book book = books.findById(bookId);
        User user = users.findById(userId);

        if(book.getHolder() == user) {
            book.setHolder(null);
            book.setReturnDate(null);
            book.setBorrowDate(null);
        }
        books.save(book);
    }

    @PreAuthorize("hasAuthority('ROLE_administrator') or hasAuthority('ROLE_librarian') or hasAuthority('ROLE_reader')")
    @PutMapping("/users/{userId}/books/{booksId}/extend")
    public void extendBorrowTime(@PathVariable("userId") int userId, @PathVariable("bookId") int bookId) {
        Book book = books.findById(bookId);
        Date returnDate = book.getReturnDate();
        dateService.extendBorrowTime(returnDate);
        book.setReturnDate(returnDate);
        books.save(book);
    }

    @PermitAll
    @GetMapping("/books/{bookId}/isAvailable")
    public boolean isAvailable(@PathVariable("bookId") int id) {
        return !(books.findById(id).isBorrowed());
    }
}
