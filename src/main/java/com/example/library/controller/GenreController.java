package com.example.library.controller;

import com.example.library.entity.Genre;
import com.example.library.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import java.util.List;

@RestController
public class GenreController {
    @Autowired
    private GenreRepository genres;

    @PermitAll
    @GetMapping("/genres")
    public List<Genre> ShowAllGenres(@RequestParam(value = "name", required = false) String name) {
        if(name == null) {
            return genres.findAll();
        }
        else {
            return genres.findByName(name);
        }
    }

    @PreAuthorize("hasAuthority('ROLE_librarian')")
    @PostMapping("/genres")
    public void addGenre(@RequestBody @Valid Genre genre) {
        genres.save(genre);
    }

    @PreAuthorize("hasAuthority('ROLE_librarian')")
    @DeleteMapping("/genres/{genreId}")
    public  void deleteGenre(@PathVariable("genreId") int id) {
        genres.delete(genres.findById(id));
    }

    @PreAuthorize("hasAuthority('ROLE_librarian')")
    @PutMapping("/genres/{genreId}")
    public void editGenre(@PathVariable("genreId") int id, @RequestBody Genre genre) {
        genre.setId(id);
        genres.save(genre);
    }
}
