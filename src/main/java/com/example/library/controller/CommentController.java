package com.example.library.controller;

import com.example.library.entity.Book;
import com.example.library.entity.Comment;
import com.example.library.entity.User;
import com.example.library.repository.BookRepository;
import com.example.library.repository.CommentRepository;
import com.example.library.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

@RestController
public class CommentController {
    @Autowired
    private CommentRepository comments;
    @Autowired
    private BookRepository books;
    @Autowired
    private UserRepository users;

    @PermitAll
    @GetMapping("/books/{bookId}/comments")
    public List<Comment> ShowAllComments(@PathVariable("bookId") int id) {
        return books.findById(id).getComments();
    }

    @PreAuthorize("hasAuthority('ROLE_administrator') or hasAuthority('ROLE_librarian') or hasAuthority('ROLE_reader')")
    @PostMapping("/users/{userId}/books/{bookId}/comments")
    public void addComment(@PathVariable("userId") int userId, @PathVariable("bookId") int bookId, @RequestBody @Valid Comment comment) {
        Book book = books.findById(bookId);
        User user = users.findById(userId);
        comment.setBook(book);
        comment.setPoster(user);
        comments.save(comment);
    }
}
