package com.example.library.controller;

import com.example.library.entity.Book;
import com.example.library.entity.User;
import com.example.library.repository.BookRepository;
import com.example.library.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.config.ProjectingArgumentResolverRegistrar;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.jws.soap.SOAPBinding;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserRepository users;

    @PreAuthorize("hasAuthority('ROLE_administrator') or hasAuthority('ROLE_librarian')")
    @GetMapping("/users")
    public List<User> findAllUsers(@RequestParam(value = "username", required = false) String username) {
        if(username == null) {
            return users.findAll();
        }
        else {
            List<User> foundUser = new ArrayList<>();
            foundUser.add(users.findByUsername(username));
            return foundUser;
        }
    }

    @PreAuthorize("hasAuthority('ROLE_administrator') or hasAuthority('ROLE_librarian')")
    @GetMapping("/users/{userId}")
    public User findUser(@RequestParam("userId") int id) {
        return users.findById(id);
    }

    @PreAuthorize("hasAuthority('ROLE_administrator')")
    @PostMapping("/users")
    public void addUser(@RequestBody @Valid User user) {
        user.setBorrowedBooks(new ArrayList<>());
        user.setComments(new ArrayList<>());
        users.save(user);
    }

    @PreAuthorize("hasAuthority('ROLE_administrator')")
    @DeleteMapping("/users")
    public void deleteUser(@RequestParam(value = "username") String username) {
        users.delete(users.findByUsername(username));
    }

    @PreAuthorize("hasAuthority('ROLE_administrator')")
    @PutMapping("/users/{userId}")
    public void updateUser(@PathVariable("userId") int id, @RequestBody User user) {
        user.setId(id);
        users.save(user);
    }
}
