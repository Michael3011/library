package com.example.library.controller;

import com.example.library.entity.Book;
import com.example.library.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class BookController {
    @Autowired
    private BookRepository books;

    @PermitAll
    @GetMapping("/books")
    public List<Book> findAllBooks(@RequestParam(value = "title", required = false) String title) {
        if(title == null) {
            return books.findAll();
        }
        else {
            return books.findByTitle(title);
        }
    }

    @PostMapping("/books")
    public void addBook(@RequestBody @Valid Book book) {
        book.setComments(new ArrayList<>());
        books.save(book);
    }

    @DeleteMapping("/books/{bookId}")
    public void deleteBook(@PathVariable("bookId") int id) {
        books.delete(books.findById(id));
    }

    @PutMapping("/books/{bookId}")
    public void updateBook(@PathVariable("bookId") int id, @RequestBody Book book) {
        book.setId(id);
        books.save(book);
    }
}
