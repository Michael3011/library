package com.example.library.service;

import com.example.library.entity.Book;
import com.example.library.exception.BookNotAvailableException;
import com.example.library.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {
    @Autowired
    private BookRepository books;

    public int getBookId(String title) throws BookNotAvailableException {
        List<Book> booksWithSameTitle = books.findByTitle(title);
        int id = 0;
        for (Book book : booksWithSameTitle) {
            if(!book.isBorrowed()) {
                id = book.getId();
            }
        }
        if(id != 0) {
            return id;
        }
        else throw new BookNotAvailableException();
    }
}
