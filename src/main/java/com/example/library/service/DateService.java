package com.example.library.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.Calendar;
import java.util.Date;

@Service
public class DateService {
    @Value("${horrow.time.in.weeks}")
    private int borrowTime;
    @Value("${additional.borrow.time.in.weeks}")
    private int additionalTime;


    public Date addBorrowDate() {
        return new Date();
    }

    public Date addReturnDate(Date borrowDate) {
        Calendar c = Calendar.getInstance();
        c.setTime(borrowDate);
        c.add(Calendar.WEEK_OF_MONTH, borrowTime);
        return  c.getTime();
    }

    public Date extendBorrowTime(Date returnDate) {
        Calendar c = Calendar.getInstance();
        c.setTime(returnDate);
        c.add(Calendar.WEEK_OF_MONTH, additionalTime);
        return  c.getTime();
    }
}
